package com.harryson.tests;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.harryson.models.Product;
import com.harryson.tests.fixtures.ProductBuilder;

import utils.DatabaseCleaner;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductTest {
	
	@BeforeClass
	public static void setup() {
		
		DatabaseCleaner.cleanAll();
	}
	
	@Test
	public void t1ShouldSaveProduct() {
		
		Product product = new ProductBuilder().simple().build();
		product.validateAndSave();
		assertTrue(true);
	}
	
	@Test
	public void t2ShouldSaveAnotherProduct() {
		
		Product product = new ProductBuilder().simple().build();
		product.validateAndSave();
		assertTrue(true);
	}
	
	@Test
	public void t3ShouldListProducts() {
		
		List<Product> products = Product.list(null);
		
		assertTrue(products.size() == 2);
	}
	
	@Test
	public void t4ShouldFindProduct() {
		
		Product product = new ProductBuilder().simple().build();
		product.validateAndSave();
		
		Product productSaved = Product.findById(product.getId());
		
		Assert.assertNotNull("Product not found", productSaved);
	}
	
//	@Test
//	TODO Fix the deleteProduct and umcomment the test
//	public void t5ShouldDeleteProduct() {
//		
//		Product product = new ProductBuilder().simple().build();
//		product.validateAndSave();
//		
//		Product productSaved = Product.findById(product.getId());
//		
//		Product.delete(productSaved.getId());
//		
//		product = Product.findById(product.getId());
//		
//		Assert.assertNull(product);
//	}
	
//	TODO Criar testes para preenchimento e save de objetos faltando dados.
	
}
