package com.harryson.tests.fixtures;

import com.harryson.models.Model;

public class DefaultModelBuilder<T extends Model> {
	
	protected T model;
	
	public DefaultModelBuilder(T model) {
		this.model = model;
	}
	
	public T build() {

		return model;
	}
}
