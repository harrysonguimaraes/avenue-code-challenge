package com.harryson.tests.fixtures;

import java.util.HashSet;

import com.harryson.models.Image;
import com.harryson.models.Product;

public class ProductBuilder extends DefaultModelBuilder<Product>{
	
	public ProductBuilder() {
		super(new Product());
	}

	public ProductBuilder simple() {
		
		model.setName("Default product");
		model.setDescription("Default description");

		return this;
	}
	
	public ProductBuilder withImage() {
		
		model.setImages(new HashSet<Image>());
		return this;
	}
	
	public ProductBuilder withParentProduct() {
		
		model.setParentProduct(new Product());
		return this;
	}

}
