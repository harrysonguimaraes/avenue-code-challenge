package utils;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.harryson.models.Image;
import com.harryson.models.Product;
import com.harryson.util.HibernateUtil;

public class DatabaseCleaner {
	
	private static SessionFactory sessFact = HibernateUtil.getSessionFactory();
	
	public static void cleanAll() {
		
		List<String> models = Arrays.asList(
				Product.class.getCanonicalName(),
				Image.class.getCanonicalName()
				);
		Session session = sessFact.getCurrentSession();
		session.beginTransaction();

		for (String model : models) {
			
			session.createQuery("delete from " + model).executeUpdate();
		}
		
		session.getTransaction().commit();
		session.close();
	}

}
