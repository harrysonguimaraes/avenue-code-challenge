package com.harryson.api;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

public abstract class Controller {
	
	final static Logger logger = Logger.getLogger(Controller.class);
	
	protected Response created() {
		
//		TODO Inform the URI of the created resource
		return Response.created(null).build();
	}
	
	protected Response returnIfNull(Object model, Object ...models) {
		if (model == null) {
			Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		if (models != null && models.length > 0) {

			for (int i = 0; i < models.length; i++) {

				if (models[i] == null) {
					Response.status(Response.Status.BAD_REQUEST).build();
				}
			}
		}
		
		return null;
	}

}
