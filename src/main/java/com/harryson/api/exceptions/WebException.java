package com.harryson.api.exceptions;

import javax.ws.rs.core.Response;

import com.harryson.util.Messages;

public class WebException extends RuntimeException {

	private static final long serialVersionUID = 7144243881308754159L;
	
	private String mensagemUsuario;
	private Response.Status status;
	
	public WebException() {
		
		this.mensagemUsuario = Messages.REQUEST_ERROR_DEFAULT;
		this.status = Response.Status.INTERNAL_SERVER_ERROR;
	}
	
	public WebException (Throwable throwable) {
		super(throwable);
		
		this.mensagemUsuario = Messages.REQUEST_ERROR_DEFAULT;
		this.status = Response.Status.INTERNAL_SERVER_ERROR;
	}
	
	public WebException(String mensagemUsuario, Response.Status httpStatusCode) {
		
		this.mensagemUsuario = mensagemUsuario;
		this.status = httpStatusCode;
	}
	
	public WebException(String error, String mensagemUsuario, Response.Status httpStatusCode) {
		
		super(error);
		this.mensagemUsuario = mensagemUsuario;
		this.status = httpStatusCode;
	}
	
	public String getMensagemUsuario() {
		return this.mensagemUsuario;
	}
	
	public Response.Status getStatus() {
		return this.status;
	}

	
}
