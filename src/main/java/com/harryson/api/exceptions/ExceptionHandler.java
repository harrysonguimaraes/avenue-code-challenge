package com.harryson.api.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.harryson.util.HibernateUtil;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		
		exception.printStackTrace();	
		HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();;
		
		if (exception instanceof WebException) {
			
			WebException webException = ((WebException) exception);
			
			return Response.status(webException.getStatus()).entity(webException.getMensagemUsuario()).build();
			
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Sorry. We could not process the request").build();
	}

}
