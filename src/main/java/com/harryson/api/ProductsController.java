package com.harryson.api;

import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.harryson.models.Product;

@Path("/v1/products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional
public class ProductsController extends Controller {
	
	@GET
	public List<Product> listProducts(@QueryParam("complete") Boolean complete) {
		
		return Product.list(complete);
	}
	
	@GET
	@Path("/{id : \\d+}")
	public Product getProduct(@PathParam("id") Long id, @QueryParam("complete") Boolean complete) {
		
		return Product.findById(id, complete);
	}
	
	@GET
	@Path("/{id : \\d+}/products")
	public List<Product> listProductsFromProduct(@PathParam("id") Long id) {
		
		List<Product> products = Product.listProductsFromProduct(id);
		return products;
	}
	
	@POST
	public Response saveProduct(Product product) {
		
		product.save();
		return created();
	}
	
	@DELETE
	@Path("/{id : \\d+}")
	public Response deleteProduct(@PathParam("id") Long id) {
		
		Product.delete(id);
		return Response.ok().build();
	}
	
	@PUT
	@Path("/{id : \\d+}")
	public Response updateProduct(@PathParam("id") Long id, Product product) {
		
		Product productSaved = Product.findById(id);
		
		productSaved.update(product);
		return Response.ok().build();
	}
}