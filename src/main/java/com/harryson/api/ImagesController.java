package com.harryson.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.harryson.models.Image;

@Path("/v1/products/{productId : \\d+}/images")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ImagesController extends Controller {
	
	@GET
	public List<Image> listImages(@PathParam("productId") Long id) {
		
		return Image.getImagesFromProduct(id);
	}
	
	@GET
	@Path("/{imageId : \\d+}")
	public Image getImage(@PathParam("imageId") Long id) {
		
		return Image.findById(id);
	}
	
	@POST
	public Response saveImage(Image image) {
		
		//TODO		
		return created();				
	}
	
	@DELETE
	@Path("/{imageId : \\d+}")
	public Response deleteImage(@PathParam("imageId") Long id) {
		
		Image.delete(id);
		return Response.ok().build();
	}
}