package com.harryson.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.Query;
import org.hibernate.Session;

import com.harryson.util.HibernateUtil;

@Entity
@Table(name="image")
public class Image extends Model {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Long id;
	
	@Column(name="type")
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public static Image findById(Long id) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		Image image = (Image) session.get(Image.class, id);

        session.close();
		return image;
	}
	
	public static List<Image> getImagesFromProduct(Long idProduto) {
		
		String hql = "FROM " + Image.class.getCanonicalName() + " i"
				+ " WHERE product.id = :id";
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		if(!session.getTransaction().isActive()) {
			session.beginTransaction();
		}
		
		Query query = session.createQuery(hql);
		query.setParameter("id", idProduto);
		
		List<Image> images = query.list();
		
		return images;
	}

	public static void delete(Long id) {
		
		Image image = Image.findById(id);
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.remove(image);
		
		session.getTransaction().commit();
		
		session.close();
	}
	
}