package com.harryson.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Query;
import org.hibernate.Session;

import com.harryson.util.HibernateUtil;

@Entity
@Table(name="product")
public class Product extends Model {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="parent_product_id", insertable = false, updatable = false)
	private Product parentProduct;
	
	@OneToMany(mappedBy="parentProduct", fetch = FetchType.EAGER)
	private Set<Product> products;
	
	@OneToMany(mappedBy="product", fetch = FetchType.EAGER, cascade=CascadeType.REMOVE)
	private Set<Image> images = new HashSet<>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public Set<Image> getImages() {
		return images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public static List<Product> list(Boolean complete) {
		
		logger.info("Buscando todos os produtos");
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		CriteriaQuery<Product> cq = session.getCriteriaBuilder().createQuery(Product.class);
		cq.from(Product.class);
		
		List<Product> products = session.createQuery(cq).getResultList();
		
		if(complete == null || complete == false) {
		
			for (Product product : products) {
				product.images = null;
				product.parentProduct = null;
			}
		}
		
        session.close();

		return products; 
	}

	public static Product findById(Long id) {
		
		return findById(id, null);
	}

	public static Product findById(Long id, Boolean complete) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		Product product = (Product) session.get(Product.class, id);
		
		if (complete == null || complete == false) {
			product.images = null;
			product.products = null;
			//TODO Fix with a serializer			
		}
		
        session.close();
		return product;
	}
	
	public static void delete(Long id) {
		
		Product product = Product.findById(id);
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		List<Image> images = Image.getImagesFromProduct(product.id);
		for (Image image : images) {
			session.remove(image);
		}
		session.clear();
		session.getTransaction().commit();
		session.beginTransaction();
		session.remove(product);
		
		session.getTransaction().commit();
		
		session.close();
	}

	public static List<Product> listProductsFromProduct(Long id) {
		
		String hql = "FROM " + Product.class.getCanonicalName() + " p"
				+ " WHERE parentProduct.id = :id";
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		
		List<Product> products = query.list();
		
		session.getTransaction().commit();
		session.close();
		
		return products;
	}
	
	public void save() {
		
		validateAndSave();
		
		for (Image image : this.images) {
			
			image.setProduct(this);
			image.validateAndSave();
		}

	}

	public void update(Product product) {
		
		this.setDescription(product.getDescription());
		this.setName(product.getName());
		if (product.parentProduct != null && product.parentProduct.id != null) {
			this.parentProduct = product.parentProduct;
		}
		
		this.validateAndUpdate();
	}
	
}