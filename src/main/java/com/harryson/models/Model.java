package com.harryson.models;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.harryson.api.exceptions.WebException;
import com.harryson.util.HibernateUtil;
import com.harryson.util.Messages;

public class Model {
	
	final static Logger logger = Logger.getLogger(Model.class);
	
	private Boolean isValid() {
		
		return true;
//		TODO Validar com base nas anotações das models
	}
	
	public Boolean validateAndSave() {
		
		if (this.isValid()) {
			
			save();
			return true;
		
		} else {
			logger.error("Objeto inválido");
//			TODO Lançar exception aqui
			return false;		
		}
	}
	
	public Boolean validateAndUpdate() {
		
		if (this.isValid()) {
			
			update();
			return true;
		
		} else {
			logger.error("Objeto inválido");
			throw new WebException(Messages.REQUEST_ERROR_DEFAULT, Response.Status.BAD_REQUEST);
			
		}
	}
	
	private Model save() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		session.save(this);
		
        session.getTransaction().commit();
        session.close();
		
		return this;
	}
	
	private Model update() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		session.update(this);
		
        session.getTransaction().commit();
        session.close();
		
		return this;
		
	}
	
	public Boolean delete() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		try {
			session.delete(this);
			
	        session.getTransaction().commit();
	        session.close();
			
			return true;
			
		} catch(Exception ex) {
			
			session.getTransaction().rollback();
			logger.error("We could not delete", ex);
			return false;
		}
	}

}
