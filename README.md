# avenue-code-challenge

Technologies:

* Java 8

* maven

* Jersey

* JUnit

* Hibernate

Setup for execution:

Clone the project:
```
git clone https://bitbucket.org/harrysonguimaraes/avenue-code-challenge.git
```

Then, inside the root project folder, on terminal, execute the command:
`mvn jetty:run`
After that, you should be able to make requests on http://localhost:8080/avenue-code-challenge

Maven will create the database in memory.
### Endpoints:
```
POST	/v1/products
{
  	"name": "Product 1",
    "description": "This is the first product on our store.",
 	"images": [
      {"type": "img.png"},
      {"type": "img2.png"}
    ] 
}
//Save a new Product

PUT		/v1/products/:id
{
  	"name": "Changed product 1",
    "description": "This is the changed description of our first product."
}
//Update an existing product

DELETE /v1/products/:id
// Delete the resource

GET		/v1/products
//List all the products (without childrens)

GET		v1/products?complete=true
//List all the products with childrens

GET		v1/products/:id
//Return a specific product (without childrens)

GET		v1/products/:id?complete=true
//Return a specific product with childrens

GET		v1/products/:id/images
//Return all images for a specific product

GET		v1/products/:idProduct/images/:idImage
//Return a specific image for a specific product

DELETE 	v1/products/:idProduct/images/:idImage
//Delete a specific image 
```

### Tests
Execute the command `mvn test` to run all the project tests.

### TODO:

- [ ] Paginate lists

- [ ] Add options to filter queries

- [ ] Add order by

- [ ] Lazy load Lists by default

- [ ] Validate user input

- [ ] Write more unit tests

- [ ] Configure Log4j


